<?php

declare(strict_types=1);

namespace Drupal\Tests\ldap_sso_auth\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

require_once __DIR__ . '/../../../ldap_sso_auth.module';

/**
 * Tests the ldap_sso_auth module.
 *
 * @group ldap_sso_auth
 */
class ModuleTest extends UnitTestCase {

  /**
   * The container.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->container = new ContainerBuilder();

    $string_translation = $this->getStringTranslationStub();
    $this->container->set('string_translation', $string_translation);

    $this->configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $this->container->set('config.factory', $this->configFactory);

    $this->requestStack = $this->createMock('Symfony\Component\HttpFoundation\RequestStack');
    $this->container->set('request_stack', $this->requestStack);

    \Drupal::setContainer($this->container);
  }

  /**
   * Tests the hook_help() implementation.
   *
   * @coversFunction ::ldap_sso_auth_help
   * @covers ::ldap_sso_auth_help
   */
  public function testHookHelpPage(): void {

    $route_match = $this->createMock('Drupal\Core\Routing\RouteMatchInterface');
    $route_name = 'help.page.ldap_sso_auth';

    $this->assertTrue(function_exists('ldap_sso_auth_help'));
    $output = ldap_sso_auth_help($route_name, $route_match);
    $this->assertIsString($output);
  }

  /**
   * Tests the hook_help() implementation.
   */
  public function testHookHelpSettings(): void {

    $route_match = $this->createMock('Drupal\Core\Routing\RouteMatchInterface');
    $route_name = 'ldap_sso_auth.admin_form';

    $this->assertTrue(function_exists('ldap_sso_auth_help'));
    $output = ldap_sso_auth_help($route_name, $route_match);
    $this->assertIsString($output);
  }

  /**
   * Tests the hook_user_logout() implementation.
   *
   * @coversFunction ::ldap_sso_auth_user_logout
   * @covers ::ldap_sso_auth_user_logout
   */
  public function testHookUserLogout(): void {
    $this->assertTrue(function_exists('ldap_sso_auth_user_logout'));

    $account = $this->createMock('Drupal\user\Entity\User');

    $config = $this->getConfigFactoryStub([
      'ldap_sso_auth.settings' => [
        'redirectOnLogout' => TRUE,
        'logoutRedirectPath' => '/user/login',
      ],
    ]);
    $this->container->set('config.factory', $config);

    $path_validator = $this->createMock('Drupal\Core\Path\PathValidatorInterface');

    $this->container->set('path.validator', $path_validator);

    $unrouted_url_assembler = $this->createMock('Drupal\Core\Utility\UnroutedUrlAssemblerInterface');
    $unrouted_url_assembler->expects($this->once())
      ->method('assemble')
      ->with('base:user/login')
      ->willReturn('/user/login');

    $this->container->set('unrouted_url_assembler', $unrouted_url_assembler);

    ob_start();
    ldap_sso_auth_user_logout($account);
    $content = ob_get_clean();

    $validate_output = <<< EOT
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="0;url='/user/login'" />

        <title>Redirecting to /user/login</title>
    </head>
    <body>
        Redirecting to <a href="/user/login">/user/login</a>.
    </body>
</html>
EOT;
    $this->assertEquals($validate_output, $content);
  }

}
