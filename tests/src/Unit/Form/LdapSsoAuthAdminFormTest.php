<?php

declare(strict_types=1);

namespace Drupal\Tests\ldap_sso_auth\Unit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ldap_sso_auth\Form\LdapSsoAuthAdminForm;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the LdapSsoAuthAdminForm class.
 *
 * @group ldap_sso_auth
 */
class LdapSsoAuthAdminFormTest extends UnitTestCase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The form under test.
   *
   * @var \Drupal\ldap_sso_auth\Form\LdapSsoAuthAdminForm
   */
  protected $form;

  /**
   * The LDAP server storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $ldapServerStorage;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $messenger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $requestStack;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configTyped;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $string_translation = $this->getStringTranslationStub();
    $container->set('string_translation', $string_translation);

    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $container->set('config.factory', $this->configFactory);

    $this->entityTypeManager = $this->createMock(EntityTypeManager::class);
    $container->set('entity_type.manager', $this->entityTypeManager);

    $url_generator = $this->createMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $container->set('url_generator', $url_generator);

    $this->messenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');
    $container->set('messenger', $this->messenger);

    $this->requestStack = $this->createMock('Symfony\Component\HttpFoundation\RequestStack');
    $container->set('request_stack', $this->requestStack);

    if (version_compare(\Drupal::VERSION, '10.2.0', '>=')) {
      $this->configTyped = $this->createMock('Drupal\Core\Config\TypedConfigManagerInterface');
      $container->set('config.typed', $this->configTyped);
    }

    $this->ldapServerStorage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $this->entityTypeManager
      ->method('getStorage')
      ->with('ldap_server')
      ->willReturn($this->ldapServerStorage);

    \Drupal::setContainer($container);

    $this->form = LdapSsoAuthAdminForm::create($container);
  }

  /**
   * Tests the create() method.
   */
  public function testCreate(): void {
    $container = $this->createMock(ContainerInterface::class);
    $container->method('get')
      ->willReturnMap([
        ['config.factory', 1, $this->configFactory],
        ['entity_type.manager', 1, $this->entityTypeManager],
        ['request_stack', 1, $this->requestStack],
        ['config.typed', 1, $this->configTyped],
      ]);

    $container->expects($this->once())
      ->method('has')
      ->with('config.typed')
      ->willReturn(version_compare(\Drupal::VERSION, '10.2.0', '>='));

    $form = LdapSsoAuthAdminForm::create($container);
    $this->assertInstanceOf(LdapSsoAuthAdminForm::class, $form);
  }

  /**
   * Tests the getFormId() method.
   */
  public function testGetFormId(): void {
    $this->assertSame('ldap_sso_auth_admin_form', $this->form->getFormId());
  }

  /**
   * Tests the buildForm() method.
   */
  public function testBuildForm(): void {
    $config = $this->createMock('Drupal\Core\Config\Config');
    $config->expects($this->exactly(10))
      ->method('get')
      ->willReturn('REMOTE_USER', 'REMOTE_USER', FALSE, TRUE, FALSE, [], [], TRUE, '/user/login', TRUE);

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('ldap_sso_auth.settings')
      ->willReturn($config);

    $server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $server->expects($this->once())
      ->method('get')
      ->with('REMOTE_USER')
      ->willReturn('example_user');
    $request = $this->createMock('Symfony\Component\HttpFoundation\Request');
    $request->server = $server;
    $this->requestStack->expects($this->once())
      ->method('getCurrentRequest')
      ->willReturn($request);

    $form_state = $this->createMock(FormStateInterface::class);
    $form = $this->form->buildForm([], $form_state);
  }

  /**
   * Tests the validateForm() method.
   */
  public function testValidateFormWithNoEnablesServers(): void {
    $query = $this->createMock('Drupal\Core\Entity\Query\QueryInterface');
    $query->expects($this->once())
      ->method('condition')
      ->with('status', 1)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('accessCheck')
      ->with(FALSE)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('execute')
      ->willReturn([]);
    $this->ldapServerStorage->expects($this->once())
      ->method('getQuery')
      ->willReturn($query);

    $this->ldapServerStorage->expects($this->once())
      ->method('loadMultiple')
      ->with([])
      ->willReturn([]);

    $form = [];
    $form_state = $this->createMock(FormStateInterface::class);
    $this->form->validateForm($form, $form_state);
  }

  /**
   * Tests the validateForm() method.
   */
  public function testValidateFormWithInvalidRedirectPath(): void {
    $query = $this->createMock('Drupal\Core\Entity\Query\QueryInterface');
    $query->expects($this->once())
      ->method('condition')
      ->with('status', 1)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('accessCheck')
      ->with(FALSE)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('execute')
      ->willReturn(['example_server']);
    $this->ldapServerStorage->expects($this->once())
      ->method('getQuery')
      ->willReturn($query);
    $example_server = $this->createMock('Drupal\ldap_servers\Entity\Server');
    $example_server->expects($this->once())
      ->method('get')
      ->with('bind_method')
      ->willReturn('user');

    $this->ldapServerStorage->expects($this->once())
      ->method('loadMultiple')
      ->with(['example_server'])
      ->willReturn([$example_server]);

    $form = [];
    $form_state = $this->createMock(FormStateInterface::class);
    $form_state->expects($this->exactly(3))
      ->method('getValue')
      ->willReturnOnConsecutiveCalls(TRUE, '', '');

    $form_state->expects($this->exactly(3))
      ->method('setErrorByName');

    $this->form->validateForm($form, $form_state);
  }

  /**
   * Tests the submitForm() method.
   */
  public function testSubmitForm(): void {
    $config = $this->createMock('Drupal\Core\Config\Config');

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('ldap_sso_auth.settings')
      ->willReturn($config);

    $form = [];
    $form_state = $this->createMock(FormStateInterface::class);
    $form_state->expects($this->once())
      ->method('getValues')
      ->willReturn([
        'ssoExcludedPaths' => '',
        'ssoExcludedHosts' => '',
        'seamlessLogin' => FALSE,
        'ssoSplitUserRealm' => TRUE,
        'ssoRemoteUserStripDomainName' => FALSE,
        'ssoVariable' => 'REMOTE_USER',
        'redirectOnLogout' => TRUE,
        'logoutRedirectPath' => '/user/login',
        'enableLoginConfirmationMessage' => TRUE,
      ]);

    $config->expects($this->exactly(9))
      ->method('set')
      ->willReturnSelf();

    $config->expects($this->once())
      ->method('save');

    $this->messenger->expects($this->once())
      ->method('addStatus');

    $this->form->submitForm($form, $form_state);
  }

}
