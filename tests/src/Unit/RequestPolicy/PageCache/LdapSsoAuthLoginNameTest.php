<?php

namespace Drupal\Tests\ldap_sso_auth\Unit\RequestPolicy\PageCache;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\Session\SessionConfigurationInterface;
use Drupal\ldap_sso_auth\RequestPolicy\PageCache\LdapSsoAuthLoginName;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

/**
 * @coversDefaultClass \Drupal\ldap_sso_auth\RequestPolicy\PageCache\LdapSsoAuthLoginName
 *
 * @group ldap_sso_auth
 */
class LdapSsoAuthLoginNameTest extends TestCase {

  /**
   * The mocked session configuration.
   *
   * @var \Drupal\Core\Session\SessionConfigurationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $sessionConfiguration;

  /**
   * The policy object under test.
   *
   * @var \Drupal\ldap_sso_auth\RequestPolicy\PageCache\LdapSsoAuthLoginName
   */
  protected $policy;

  /**
   * The mocked config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The mocked config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->sessionConfiguration = $this->createMock(SessionConfigurationInterface::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->config = $this->createMock(ImmutableConfig::class);
    $this->configFactory->expects($this->any())
      ->method('get')
      ->with('ldap_sso_auth.settings')
      ->willReturn($this->config);

    $this->policy = new LdapSsoAuthLoginName($this->sessionConfiguration, $this->configFactory);
  }

  /**
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $this->assertInstanceOf(LdapSsoAuthLoginName::class, $this->policy);
  }

  /**
   * Tests the check() method when there is no session.
   *
   * @covers ::check
   */
  public function testCheckNoSession(): void {
    $request = new Request([], [], [], [], [], ['REMOTE_USER' => 'user1']);
    $this->sessionConfiguration->expects($this->once())
      ->method('hasSession')
      ->with($request)
      ->willReturn(FALSE);

    $this->config->expects($this->once())
      ->method('get')
      ->with('ssoVariable')
      ->willReturn('REMOTE_USER');

    $this->assertSame(RequestPolicyInterface::DENY, $this->policy->check($request));
  }

  /**
   * Tests the check() method when there is a session and it's empty.
   *
   * @covers ::check
   */
  public function testCheckSessionWithoutSsoVariable(): void {
    $request = new Request([], [], [], [], [], []);
    $this->sessionConfiguration->expects($this->once())
      ->method('hasSession')
      ->with($request)
      ->willReturn(FALSE);

    $this->config->expects($this->once())
      ->method('get')
      ->with('ssoVariable')
      ->willReturn('REMOTE_USER');

    $this->assertSame(RequestPolicyInterface::ALLOW, $this->policy->check($request));
  }

  /**
   * Tests the check() method when there is a session and it's empty.
   *
   * @covers ::check
   */
  public function testCheckEmptySession(): void {
    $request = new Request([], [], [], [], [], []);
    $this->sessionConfiguration->expects($this->once())
      ->method('hasSession')
      ->with($request)
      ->willReturn(TRUE);

    $this->assertSame(RequestPolicyInterface::DENY, $this->policy->check($request));
  }

  /**
   * Tests the check() method when there is a session and it's not empty.
   *
   * @covers ::check
   */
  public function testCheckNonEmptySession() {
    // Create a request object with a session.
    $request = Request::create('/example');
    $session = new Session(new MockArraySessionStorage());
    $session->set('example_key', 'example_value');
    $request->setSession($session);

    // Create a mock session configuration object.
    $sessionConfiguration = $this->createMock(SessionConfigurationInterface::class);
    $sessionConfiguration->expects($this->any())
      ->method('hasSession')
      ->with($request)
      ->willReturn(TRUE);

    // Create an instance of the LdapSsoAuthLoginName object and invoke the
    // check method with the request object.
    $loginNamePolicy = new LdapSsoAuthLoginName($sessionConfiguration, $this->configFactory);
    $result = $loginNamePolicy->check($request);

    // Assert that the check method returns the expected value.
    $this->assertEquals(RequestPolicyInterface::DENY, $result);
  }

}
