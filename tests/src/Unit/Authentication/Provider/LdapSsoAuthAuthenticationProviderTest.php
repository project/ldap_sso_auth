<?php

namespace Drupal\Tests\ldap_sso_auth\Unit\Authentication\Provider;

use Drupal\ldap_sso_auth\Authentication\Provider\LdapSsoAuthAuthenticationProvider;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Tests the LdapSsoAuthAuthenticationProvider class.
 *
 * @coversDefaultClass \Drupal\ldap_sso_auth\Authentication\Provider\LdapSsoAuthAuthenticationProvider
 *
 * @group ldap_sso_auth
 */
class LdapSsoAuthAuthenticationProviderTest extends UnitTestCase {

  /**
   * The mocked LdapSsoAuthAuthenticationInterface object.
   *
   * @var \Drupal\ldap_sso_auth\LdapSsoAuthAuthenticationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $ldapSsoAuth;

  /**
   * The LdapSsoAuthAuthenticationProvider object.
   *
   * @var \Drupal\ldap_sso_auth\Authentication\Provider\LdapSsoAuthAuthenticationProvider
   */
  protected $provider;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setup();
    $this->ldapSsoAuth = $this->getMockForAbstractClass('\Drupal\ldap_sso_auth\LdapSsoAuthAuthenticationInterface');
    $this->provider = new LdapSsoAuthAuthenticationProvider($this->ldapSsoAuth);
  }

  /**
   * Tests the applies() method.
   *
   * @covers ::applies
   * @covers ::__construct
   */
  public function testApplies() {

    $this->provider = new LdapSsoAuthAuthenticationProvider($this->ldapSsoAuth);
    $request = new Request();
    $this->ldapSsoAuth->expects($this->once())
      ->method('applies')
      ->with($request)
      ->willReturn(TRUE);
    $this->assertTrue($this->provider->applies($request));
  }

  /**
   * Tests the authenticate() method.
   *
   * @covers ::authenticate
   */
  public function testAuthenticate() {
    $request = new Request();
    $authentication = ['uid' => 12345];
    $this->ldapSsoAuth->expects($this->once())
      ->method('authenticate')
      ->with($request)
      ->willReturn($authentication);
    $this->assertEquals($authentication, $this->provider->authenticate($request));
  }

  /**
   * Tests the cleanup() method.
   *
   * @covers ::cleanup
   */
  public function testCleanup() {
    $request = new Request();
    $this->assertNull($this->provider->cleanup($request));
  }

  /**
   * Tests the handleException() method.
   *
   * @covers ::handleException
   */
  public function testHandleException() {
    $accessDeniedException = new AccessDeniedHttpException();
    $exceptionEvent = new ExceptionEvent(
      $this->createMock('\Symfony\Component\HttpKernel\HttpKernelInterface'),
      $this->createMock(Request::class),
      0,
      $accessDeniedException
    );

    $this->assertTrue($this->provider->handleException($exceptionEvent));

    $unauthorizedException = new UnauthorizedHttpException('Invalid consumer origin.', $accessDeniedException);
    $this->provider->handleException($exceptionEvent);
    $this->assertEquals($unauthorizedException, $exceptionEvent->getThrowable());
  }

}
