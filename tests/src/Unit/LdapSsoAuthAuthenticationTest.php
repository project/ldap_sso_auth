<?php

namespace Drupal\Tests\ldap_sso_auth\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\ldap_sso_auth\LdapSsoAuthAuthentication;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

require_once DRUPAL_ROOT . '/core/modules/user/user.module';

/**
 * Tests the LdapSsoAuthAuthentication service.
 *
 * @group ldap_sso_auth
 * @coversDefaultClass \Drupal\ldap_sso_auth\LdapSsoAuthAuthentication
 */
class LdapSsoAuthAuthenticationTest extends UnitTestCase {

  /**
   * Tests LdapSsoAuthAuthentication::applies() method.
   *
   * @covers ::applies
   * @dataProvider providerApplies
   */
  public function testApplies($session, $pathInfo, $ssoVariable, $ssoValue, $expectedResult) {
    $container = new ContainerBuilder();
    $config = [
      'ldap_sso_auth.settings' => [
        'ssoVariable' => $ssoVariable,
        'ssoExcludedPaths' => [
          '/path/excluded',
          '<front>',
        ],
        'logoutRedirectPath' => '/user/login',
      ],
      'system.site' => [
        'frontpage' => '/frontpage',
      ],
    ];

    $config_factory = $this->getConfigFactoryStub($config);
    $entity_type_manager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $ldap_authentication_login_validator = $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso');
    $ldap_log = $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog');

    $request = $this->createMock(Request::class);
    $request->method('getSession')->willReturn($session);
    $request->method('getPathInfo')->willReturn($pathInfo);
    $request->server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $request->server->method('get')->with($ssoVariable)->willReturn($ssoValue);

    $service = new LdapSsoAuthAuthentication(
      $config_factory,
      $entity_type_manager,
      $ldap_authentication_login_validator,
      $ldap_log
    );

    $this->assertEquals($expectedResult, $service->applies($request));
  }

  /**
   * Tests LdapSsoAuthAuthentication::applies() method.
   *
   * @covers ::applies
   */
  public function testAppliesFalse() {
    $session = $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface');
    $container = new ContainerBuilder();
    $config = [
      'ldap_sso_auth.settings' => [
        'ssoVariable' => 'REMOTE_USER',
        'ssoExcludedPaths' => [
          '/path/excluded',
          '<front>',
        ],
        'logoutRedirectPath' => '/user/login',
      ],
      'system.site' => [
        'frontpage' => '/frontpage',
      ],
    ];
    $path_info = '/path';
    $config_factory = $this->getConfigFactoryStub($config);
    $entity_type_manager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $ldap_authentication_login_validator = $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso');
    $ldap_log = $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog');

    $request = $this->createMock(Request::class);
    $request->method('getSession')->willReturn($session);
    $request->method('getPathInfo')->willReturn($path_info);
    $request->server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    // $request->server->method('get')->with($ssoVariable)->willReturn();
    $service = new LdapSsoAuthAuthentication(
      $config_factory,
      $entity_type_manager,
      $ldap_authentication_login_validator,
      $ldap_log
    );

    $this->assertFalse($service->applies($request));
  }

  /**
   * Provides data to test LdapSsoAuthAuthentication::applies() method.
   */
  public function providerApplies() {
    return [
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/path',
        'SSO_VAR',
        'SSO_VALUE',
        TRUE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/user/login',
        'SSO_VAR',
        'SSO_VALUE',
        FALSE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/path',
        'SSO_VAR',
        'SSO_VALUE',
        TRUE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/path/excluded',
        'SSO_VAR',
        'SSO_VALUE',
        FALSE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/path/excluded',
        'SSO_VAR',
        'SSO_VALUE',
        FALSE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/frontpage',
        'SSO_VAR',
        'SSO_VALUE',
        FALSE,
      ],
      [
        $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
        '/frontpage',
        'SSO_VAR',
        'SSO_VALUE',
        FALSE,
      ],
    ];
  }

  /**
   * Tests LdapSsoAuthAuthentication::setRequest() method.
   *
   * @covers ::setRequest
   * @covers ::__construct
   */
  public function testSetRequest(): void {
    $request = $this->createMock(Request::class);
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $service->setRequest($request);
  }

  /**
   * Tests LdapSsoAuthAuthentication::authenticate() method.
   *
   * @covers ::authenticate
   */
  public function testAuthenticateNoSsoVariableSet(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );
    $server = $this->createMock('Drupal\ldap_servers\Entity\Server');
    $server->method('get')->willReturn(NULL);
    $request = $this->createMock(Request::class);
    $request->server = $server;

    $account = $service->authenticate($request);
    $this->assertNull($account);
  }

  /**
   * Tests LdapSsoAuthAuthentication::authenticate() method.
   *
   * @covers ::authenticate
   */
  public function testAuthenticateNotSuccessful(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoVariable' => 'REMOTE_USER',
          'ssoSplitUserRealm' => TRUE,
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );
    $server = $this->createMock('Drupal\ldap_servers\Entity\Server');
    $server->expects($this->exactly(2))
      ->method('get')
      ->with('REMOTE_USER')
      ->willReturn('DOMAIN\user1');
    $request = $this->createMock(Request::class);
    $request->server = $server;

    $account = $service->authenticate($request);
    $this->assertFalse($account);
  }

  /**
   * Tests LdapSsoAuthAuthentication::authenticate() method.
   *
   * @covers ::authenticate
   */
  public function testAuthenticate(): void {

    $container = new ContainerBuilder();

    $account = $this->createMock('Drupal\user\UserInterface');

    $current_user = $this->createMock('Drupal\Core\Session\AccountProxy');
    $current_user->expects($this->once())
      ->method('setAccount')
      ->with($account);
    $container->set('current_user', $current_user);

    $datetime_time = $this->createMock('Drupal\Component\Datetime\TimeInterface');
    $container->set('datetime.time', $datetime_time);

    $module_handler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $container->set('module_handler', $module_handler);

    $entity_type_manager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $container->set('entity_type.manager', $entity_type_manager);

    $user_storage = $this->createMock('Drupal\user\UserStorageInterface');
    $user_storage->expects($this->once())
      ->method('updateLastLoginTimestamp')
      ->with($account);

    $entity_type_manager->expects($this->once())
      ->method('getStorage')
      ->with('user')
      ->willReturn($user_storage);

    $session = $this->createMock('Symfony\Component\HttpFoundation\Session\SessionInterface');
    $container->set('session', $session);

    $logger_factory = $this->createMock('Drupal\Core\Logger\LoggerChannelFactoryInterface');
    $logger = $this->createMock('Psr\Log\LoggerInterface');
    $logger_factory->expects($this->once())
      ->method('get')
      ->with('user')
      ->willReturn($logger);

    $container->set('logger.factory', $logger_factory);

    \Drupal::setContainer($container);

    $validator = $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso');
    $validator->expects($this->exactly(3))
      ->method('getDrupalUser')
      ->willReturn($account);

    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoVariable' => 'REMOTE_USER',
          'ssoSplitUserRealm' => TRUE,
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $validator,
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );
    $server = $this->createMock('Drupal\ldap_servers\Entity\Server');
    $server->expects($this->exactly(2))
      ->method('get')
      ->with('REMOTE_USER')
      ->willReturn('user1@example.org');
    $request = $this->createMock(Request::class);
    $request->server = $server;

    $account = $service->authenticate($request);
    $this->assertTrue($account instanceof UserInterface);
  }

  /**
   * Tests LdapSsoAuthAuthentication::defaultPathsToExclude() method.
   *
   * @covers ::defaultPathsToExclude
   */
  public function testDefaultPathsToExclude(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertEquals([
      '/admin/config/search/clean-urls/check',
      '/user/login/sso',
      '/user/login',
      '/user/logout',
      '/user/password',
    ], $service->defaultPathsToExclude());
  }

  /**
   * Tests LdapSsoAuthAuthentication::splitUserNameRealm() method.
   *
   * @covers ::splitUserNameRealm
   */
  public function testSplitUserNameRealm(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertEquals(['DOMAIN\user1', NULL], $service->splitUserNameRealm('DOMAIN\user1'));
  }

  /**
   * Tests LdapSsoAuthAuthentication::splitUserNameRealm() method.
   *
   * @covers ::splitUserNameRealm
   */
  public function testSplitUserNameRealmUserPrincipal(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertEquals(['user1', 'example.org'], $service->splitUserNameRealm('user1@example.org'));
  }

  /**
   * Tests LdapSsoAuthAuthentication::stripDomainName() method.
   *
   * @covers ::stripDomainName
   */
  public function testStripSamDomainName(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertEquals('user1', $service->stripDomainName('DOMAIN\user1'));
  }

  /**
   * Tests LdapSsoAuthAuthentication::stripDomainName() method.
   *
   * @covers ::stripDomainName
   */
  public function testStripUserPrincipalDomainName(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertEquals('user1', $service->stripDomainName('user1@example.org'));
  }

  /**
   * Tests LdapSsoAuthAuthentication::validateUser() method.
   *
   * @covers ::validateUser
   */
  public function testValidateInvalidUser(): void {
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $this->assertFalse($service->validateUser('user1'));
  }

  /**
   * Tests LdapSsoAuthAuthentication::validateUser() method.
   *
   * @covers ::validateUser
   */
  public function testValidateUser(): void {

    $validator = $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso');
    $validator->expects($this->exactly(3))
      ->method('getDrupalUser')
      ->willReturn($this->createMock('Drupal\user\UserInterface'));

    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $validator,
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $account = $service->validateUser('user1');
    $this->assertInstanceOf('Drupal\user\UserInterface', $account);
  }

  /**
   * Tests LdapSsoAuthAuthentication::validateUser() method.
   *
   * @covers ::loginRemoteUser
   */
  public function testLoginRemoteUser(): void {
    $validator = $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso');
    $validator->expects($this->exactly(3))
      ->method('getDrupalUser')
      ->willReturn($this->createMock('Drupal\user\UserInterface'));

    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoRemoteUserStripDomainName' => TRUE,
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $validator,
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $account = $service->loginRemoteUser('user1', 'realm1');
    $this->assertInstanceOf('Drupal\user\UserInterface', $account);
  }

  /**
   * Tests LdapSsoAuthAuthentication::checkExcludePath() method.
   *
   * @covers ::checkExcludePath
   */
  public function testCheckExcludePathIndexPhp(): void {

    $container = new ContainerBuilder();

    \Drupal::setContainer($container);

    $server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $server->expects($this->once())
      ->method('get')
      ->with('PHP_SELF')
      ->willReturn('/index.php');

    $request = $this->createMock(Request::class);
    $request->expects($this->once())
      ->method('getPathInfo')
      ->willReturn('node/1');
    $request->server = $server;
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoExcludedPaths' => [
            '/path/excluded',
            '<front>',
          ],
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $service->setRequest($request);

    $this->assertFalse($service->checkExcludePath());
  }

  /**
   * Tests LdapSsoAuthAuthentication::checkExcludePath() method.
   *
   * @covers ::checkExcludePath
   */
  public function testCheckExcludePathIndexPhpExclude(): void {

    $container = new ContainerBuilder();

    \Drupal::setContainer($container);

    $server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $server->expects($this->once())
      ->method('get')
      ->with('PHP_SELF')
      ->willReturn('/index.php');

    $request = $this->createMock(Request::class);
    $request->expects($this->once())
      ->method('getPathInfo')
      ->willReturn('/user/logout');
    $request->server = $server;
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoExcludedPaths' => [
            '/path/excluded',
            '<front>',
          ],
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $service->setRequest($request);

    $this->assertTrue($service->checkExcludePath());
  }

  /**
   * Tests LdapSsoAuthAuthentication::checkExcludePath() method.
   *
   * @covers ::checkExcludePath
   */
  public function testCheckExcludePath(): void {

    $container = new ContainerBuilder();

    \Drupal::setContainer($container);

    $server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $server->expects($this->exactly(2))
      ->method('get')
      ->with('PHP_SELF')
      ->willReturn('/cron.php');

    $request = $this->createMock(Request::class);
    $request->server = $server;
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoExcludedPaths' => [
            '/path/excluded',
            '<front>',
          ],
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $service->setRequest($request);

    $this->assertFalse($service->checkExcludePath());
  }

  /**
   * Tests LdapSsoAuthAuthentication::checkExcludePath() method.
   *
   * @covers ::checkExcludePath
   */
  public function testCheckExcludePathExcludeHost(): void {

    $container = new ContainerBuilder();

    \Drupal::setContainer($container);

    $server = $this->createMock('Symfony\Component\HttpFoundation\ServerBag');
    $server->expects($this->exactly(3))
      ->method('get')
      ->willReturn('/user/logout', '/user/logout', 'example.org');

    $request = $this->createMock(Request::class);
    $request->server = $server;
    $service = new LdapSsoAuthAuthentication(
      $this->getConfigFactoryStub([
        'ldap_sso_auth.settings' => [
          'ssoExcludedPaths' => [
            '/path/excluded',
            '<front>',
          ],
          'ssoExcludedHosts' => [
            'host' => 'example.org',
          ],
        ],
        'system.site' => ['frontpage' => '/frontpage'],
      ]),
      $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('Drupal\ldap_authentication\Controller\LoginValidatorSso'),
      $this->createMock('Drupal\ldap_servers\Logger\LdapDetailLog')
    );

    $service->setRequest($request);

    $this->assertTrue($service->checkExcludePath());
  }

}
