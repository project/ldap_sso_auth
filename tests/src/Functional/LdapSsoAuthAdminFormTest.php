<?php

namespace Drupal\Tests\ldap_sso_auth\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the LDAP SSO settings form.
 *
 * @group ldap_sso_auth
 */
class LdapSsoAuthAdminFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ldap_sso_auth'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create and log in an administrator user.
    $admin_user = $this->createUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests submitting the LDAP SSO settings form.
   */
  public function testLdapSsoAuthAdminForm() {

    // Test if the form is loaded.
    $this->drupalGet(Url::fromRoute('ldap_sso_auth.admin_form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('LDAP SSO Auth | Drupal');

    // Test if the form elements are loaded.
    $this->assertEquals('checkbox', $this->getSession()->getPage()
      ->find('css', '#edit-seamlesslogin')->getAttribute('type'));
    $this->assertSession()->checkboxNotChecked('edit-seamlesslogin');

    $this->assertEquals('checkbox', $this->getSession()->getPage()
      ->find('css', '#edit-ssosplituserrealm')->getAttribute('type'));
    $this->assertSession()->checkboxChecked('edit-ssosplituserrealm');

    $this->assertEquals('checkbox', $this->getSession()->getPage()
      ->find('css', '#edit-ssoremoteuserstripdomainname')->getAttribute('type'));
    $this->assertSession()->checkboxNotChecked('edit-ssoremoteuserstripdomainname');

    $this->assertSession()->fieldExists('Server variable containing the user');
    $this->assertSession()->fieldValueEquals('ssoVariable', 'REMOTE_USER');

    $this->assertSession()->fieldExists('SSO Excluded Paths');
    $this->assertSession()->fieldValueEquals('ssoExcludedPaths', '');

    $this->assertSession()->fieldExists('SSO Excluded Hosts');
    $this->assertSession()->fieldValueEquals('ssoExcludedHosts', '');

    $this->assertEquals('checkbox', $this->getSession()->getPage()
      ->find('css', '#edit-redirectonlogout')->getAttribute('type'));
    $this->assertSession()->checkboxChecked('edit-redirectonlogout');

    $this->assertSession()->fieldExists('Logout redirect path');
    $this->assertSession()->fieldValueEquals('logoutRedirectPath', '/user/login');

    $this->assertEquals('checkbox', $this->getSession()->getPage()
      ->find('css', '#edit-enableloginconfirmationmessage')->getAttribute('type'));
    $this->assertSession()->checkboxChecked('edit-enableloginconfirmationmessage');

    // Submit the form with valid values.
    $edit = [
      'seamlessLogin' => TRUE,
      'ssoSplitUserRealm' => FALSE,
      'ssoRemoteUserStripDomainName' => TRUE,
      'ssoVariable' => 'USER_REMOTE',
      'ssoExcludedPaths' => '/user/login' . PHP_EOL . '/user/register',
      'ssoExcludedHosts' => 'localhost' . PHP_EOL . '127.0.0.1',
      'redirectOnLogout' => FALSE,
      'logoutRedirectPath' => '/user',
      'enableLoginConfirmationMessage' => FALSE,
    ];
    $this->submitForm($edit, 'Save');

    // Verify that the form was submitted successfully.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Test if the form elements changed.
    $this->assertSession()->checkboxChecked('edit-seamlesslogin');
    $this->assertSession()->checkboxNotChecked('edit-ssosplituserrealm');
    $this->assertSession()->checkboxChecked('edit-ssoremoteuserstripdomainname');
    $this->assertSession()->fieldValueEquals('ssoVariable', 'USER_REMOTE');
    $this->assertSession()->fieldValueEquals('ssoExcludedPaths', '/user/login
/user/register');
    $this->assertSession()->fieldValueEquals('ssoExcludedHosts', 'localhost
127.0.0.1');

    $this->assertSession()->checkboxNotChecked('edit-redirectonlogout');
    $this->assertSession()->fieldValueEquals('logoutRedirectPath', '/user');
    $this->assertSession()->checkboxNotChecked('edit-enableloginconfirmationmessage');
  }

}
