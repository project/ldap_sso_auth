<?php

namespace Drupal\Tests\ldap_sso_auth\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the ldap_sso_auth_user_logout() function.
 *
 * @group ldap_sso_auth
 */
class LdapSsoAuthUserLogoutTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ldap_sso_auth'];

  /**
   * Tests redirect on logout.
   */
  public function testRedirectOnLogout() {
    $config = $this->config('ldap_sso_auth.settings');
    $config->set('seamlessLogin', TRUE);
    $config->set('redirectOnLogout', TRUE);
    $config->set('logoutRedirectPath', '/user/login');
    $config->save();

    $expected_redirect_url = Url::fromUserInput('/user/login');

    $account = $this->drupalCreateUser();

    $this->drupalLogin($account);

    $this->assertSession()->addressNotEquals($expected_redirect_url);

    $this->drupalLogout();

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($expected_redirect_url);
  }

}
