<?php

declare(strict_types=1);

namespace Drupal\Tests\ldap_sso_auth\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

require_once __DIR__ . '/../../../ldap_sso_auth.module';

/**
 * Tests the admin listing fallback when views is not enabled.
 *
 * @group authorization
 */
class ModuleTest extends EntityKernelTestBase {

  /**
   * The list builder.
   *
   * @var \Drupal\Core\Entity\EntityListBuilderInterface
   */
  protected $listBuilder;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ldap_servers',
    'externalauth',
    'ldap_authentication',
    'ldap_user',
    'ldap_query',
    'ldap_sso_auth',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['ldap_sso_auth']);
  }

  /**
   * Tests that the correct cache contexts are set.
   *
   * @covers ::ldap_sso_auth_help
   */
  public function testHookHelp() {

    // $output = ldap_sso_auth_help('help.page.ldap_sso_auth', );
  }

  /**
   * Tests the hook_user_logout() implementation.
   *
   * @covers ::ldap_sso_auth_user_logout
   */
  public function testHookUserLogout() {
    $user = $this->createUser();

    ob_start();
    ldap_sso_auth_user_logout($user);
    $output = ob_get_clean();
    ob_end_clean();
    $validate_output = <<< EOT
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="0;url='/user/login'" />

        <title>Redirecting to /user/login</title>
    </head>
    <body>
        Redirecting to <a href="/user/login">/user/login</a>.
    </body>
</html>
EOT;
    $this->assertEquals($validate_output, $output);
  }

}
