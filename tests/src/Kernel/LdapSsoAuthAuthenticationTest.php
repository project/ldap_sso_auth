<?php

declare(strict_types=1);

namespace Drupal\Tests\ldap_sso_auth\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ldap_sso_auth\LdapSsoAuthAuthentication;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests the LdapSsoAuthAuthentication class.
 *
 * @group ldap_sso_auth
 * @coversDefaultClass \Drupal\ldap_sso_auth\LdapSsoAuthAuthentication
 */
class LdapSsoAuthAuthenticationTest extends KernelTestBase {

  /**
   * The LDAP SSO authentication service.
   *
   * @var \Drupal\ldap_sso_auth\LdapSsoAuthAuthentication
   */
  protected $ldapSsoAuthAuthentication;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ldap_servers',
    'ldap_authentication',
    'ldap_user',
    'externalauth',
    'ldap_query',
    'ldap_sso_auth',
  ];

  /**
   * {@inheritdoc}
   *
   * @covers ::__construct
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['ldap_sso_auth']);

    $this->ldapSsoAuthAuthentication = new LdapSsoAuthAuthentication(
      $this->container->get('config.factory'),
      $this->container->get('entity_type.manager'),
      $this->container->get('ldap_authentication.login_validator_sso'),
      $this->container->get('ldap.detail_log'));
  }

  /**
   * Tests the setRequest() method.
   *
   * @covers ::setRequest
   */
  public function testSetRequest(): void {

    $session = $this->container->get('session');
    $request = new Request();
    $request->setSession($session);

    $result = $this->ldapSsoAuthAuthentication->setRequest($request);
    $this->assertNull($result);
  }

  /**
   * Tests the applies() method.
   *
   * @covers ::applies
   */
  public function testApplies(): void {
    $session = $this->container->get('session');
    $request = new Request();
    $request->setSession($session);

    $this->assertFalse($this->ldapSsoAuthAuthentication->applies($request));
  }

  /**
   * Tests the stringDomainName() method.
   *
   * @covers ::stripDomainName
   */
  public function testStringDomainNameUserPrincipal(): void {
    $this->assertEquals('user', $this->ldapSsoAuthAuthentication->stripDomainName('user@example.com'));
  }

  /**
   * Tests the stringDomainName() method.
   *
   * @covers ::stripDomainName
   */
  public function testStringDomainNameSamNotation(): void {
    $this->assertEquals('user', $this->ldapSsoAuthAuthentication->stripDomainName('example\user'));
  }

  /**
   * Tests the defaultPathsToExclude() method.
   *
   * @covers ::defaultPathsToExclude
   */
  public function testDefaultPathsToExclude(): void {
    $actual = $this->ldapSsoAuthAuthentication->defaultPathsToExclude();
    $expects = [
      '/admin/config/search/clean-urls/check',
      '/user/login/sso',
      '/user/login',
      '/user/logout',
      '/user/password',
    ];

    $this->assertEquals($expects, $actual);
  }

}
