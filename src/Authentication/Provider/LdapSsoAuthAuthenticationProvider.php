<?php

namespace Drupal\ldap_sso_auth\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\ldap_sso_auth\LdapSsoAuthAuthenticationInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Provides LDAP SSO Authentication.
 */
class LdapSsoAuthAuthenticationProvider implements AuthenticationProviderInterface {

  /**
   * The request.
   *
   * @var \Drupal\ldap_sso_auth\LdapSsoAuthAuthenticationInterface
   */
  protected $ldapSsoAuth;

  /**
   * Constructs a LDAP SSO Auth authentication provider object.
   *
   * @param \Drupal\ldap_sso_auth\LdapSsoAuthAuthenticationInterface $ldap_sso_auth_auth
   *   The service injection container.
   */
  public function __construct(LdapSsoAuthAuthenticationInterface $ldap_sso_auth_auth) {
    $this->ldapSsoAuth = $ldap_sso_auth_auth;
  }

  /**
   * Checks whether suitable authentication credentials are on the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE if authentication credentials suitable for this provider are on the
   *   request, FALSE otherwise.
   */
  public function applies(Request $request) {
    return $this->ldapSsoAuth->applies($request);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    return $this->ldapSsoAuth->authenticate($request);
  }

  /**
   * {@inheritdoc}
   */
  public function cleanup(Request $request) {}

  /**
   * {@inheritdoc}
   */
  public function handleException(ExceptionEvent $event) {
    $exception = $event->getThrowable();
    if ($exception instanceof AccessDeniedHttpException) {
      $event->setThrowable(
        new UnauthorizedHttpException('Invalid consumer origin.', $exception)
      );
      return TRUE;
    }
    return FALSE;
  }

}
