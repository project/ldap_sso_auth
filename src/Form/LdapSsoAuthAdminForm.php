<?php

namespace Drupal\ldap_sso_auth\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the configuration form SSO under LDAP configuration.
 */
final class LdapSsoAuthAdminForm extends ConfigFormBase {

  /**
   * The LDAP server storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * LdapSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typed_config
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager, RequestStack $request_stack, ?TypedConfigManagerInterface $typed_config = NULL) {

    if (version_compare(\Drupal::VERSION, '10.2.0', '>=')) {
      parent::__construct($config_factory, $typed_config);
    }
    else {
      parent::__construct($config_factory);
    }

    $this->storage = $entity_type_manager->getStorage('ldap_server');
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->has('config.typed') ? $container->get('config.typed') : NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ldap_sso_auth_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ldap_sso_auth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('ldap_sso_auth.settings');

    $form['ssoVariable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server variable containing the user'),
      '#description' => $this->t('This is usually REMOTE_USER or REDIRECT_REMOTE_USER.'),
      '#default_value' => $config->get('ssoVariable'),
    ];
    $sso_variable = $config->get('ssoVariable');
    $server = $this->requestStack->getCurrentRequest()->server;

    $sso_value = $server->get($sso_variable) ?? '<em>NULL</em>';
    $output = '<p>$_SERVER[\'' . $sso_variable . '\'] = ' . $sso_value . '</p>';
    if ($sso_variable != 'REMOTE_USER') {
      $remote_user_value = $server->get('REMOTE_USER') ?? '<em>NULL</em>';
      $output .= '<p>$_SERVER[\'REMOTE_USER\'] = ' . $remote_user_value . '</p>';
    }
    $form['ssoVariable']['#description'] .= $output;

    $form['seamlessLogin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Turn on automated single sign-on'),
      '#description' => $this->t('This requires that you have operational NTLM or Kerberos authentication turned on for at least the path /user/login/sso (enabling it for the entire host works too).'),
      '#default_value' => $config->get('seamlessLogin'),
    ];

    $form['ssoSplitUserRealm'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Split user name and realm'),
      '#description' => $this->t("If your users are shown as user@realm, you need to enable this. <br><strong>This is the default for mod_auth_kerb but not mod_auth_sspi.</strong>"),
      '#default_value' => $config->get('ssoSplitUserRealm'),
    ];

    $form['ssoRemoteUserStripDomainName'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strip REMOTE_USER of domain name'),
      '#description' => $this->t('Use this if you get users in the form of user@realm via SSO and also want to authenticate manually without a realm and avoid duplicate or conflicting accounts.'),
      '#default_value' => $config->get('ssoRemoteUserStripDomainName'),
    ];

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Advanced settings'),
      '#title_display' => 'invisible',
    ];

    $form['excluded_paths'] = [
      '#type' => 'details',
      '#title' => $this->t('Excluded Paths'),
      '#group' => 'advanced',
    ];

    $form['excluded_paths']['ssoExcludedPaths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SSO Excluded Paths'),
      '#description' => $this->t("Common paths to exclude from SSO are for example cron.php.<br>This module already excludes some system paths, such as /user/login.<br>Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard.<br>Example paths are %blog for the blog page and %blog-wildcard for all pages below it. %front is the front page.",
        ['%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>']),
      '#default_value' => implode(PHP_EOL, $config->get('ssoExcludedPaths')),
    ];

    $form['excluded_hosts'] = [
      '#type' => 'details',
      '#title' => $this->t('Excluded Hosts'),
      '#group' => 'advanced',
    ];

    $form['excluded_hosts']['ssoExcludedHosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SSO Excluded Hosts'),
      '#description' => $this->t('If your site is accessible via multiple hostnames, you may only want
        the LDAP SSO module to authenticate against some of them.<br>Enter one host per line.'),
      '#default_value' => implode(PHP_EOL, $config->get('ssoExcludedHosts')),
    ];

    $form['login'] = [
      '#type' => 'details',
      '#title' => $this->t('Login customization'),
      '#group' => 'advanced',
    ];

    $form['login']['redirectOnLogout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect users on logout'),
      '#description' => $this->t('Recommended to be set for most sites to a non-SSO path. Can cause issues with immediate cookie invalidation and automated SSO.'),
      '#default_value' => $config->get('redirectOnLogout'),
    ];

    $form['login']['logoutRedirectPath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logout redirect path'),
      '#description' => $this->t('An internal Drupal path that users will be redirected to on logout'),
      '#default_value' => $config->get('logoutRedirectPath'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="redirectOnLogout"]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="redirectOnLogout"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['login']['enableLoginConfirmationMessage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show a confirmation message on successful login'),
      '#default_value' => $config->get('enableLoginConfirmationMessage'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $query_result = $this->storage
      ->getQuery()
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();
    $enabled_servers = $this->storage->loadMultiple($query_result);
    /** @var \Drupal\ldap_servers\ServerInterface $server */
    foreach ($enabled_servers as $server) {
      if ($server->get('bind_method') == 'user' || $server->get('bind_method') == 'anon_user') {
        $form_state->setErrorByName('ssoEnabled', $this->t("Single sign-on is not valid with the server %sid because that server configuration uses %bind_method. Since the user's credentials are never available to this module with single sign-on enabled, there is no way for the ldap module to bind to the ldap server with credentials.",
          [
            '%sid' => $server->id(),
            '%bind_method' => $server->getFormattedBind(),
          ]
        ));
      }
    }

    if ($form_state->getValue('redirectOnLogout')) {
      if ($form_state->getValue('logoutRedirectPath') == '') {
        $form_state->setErrorByName('logoutRedirectPath', $this->t('Redirect logout path cannot be blank'));
      }

      try {
        Url::fromUserInput($form_state->getValue('logoutRedirectPath'));
      }
      catch (\InvalidArgumentException $ex) {
        $form_state->setErrorByName('logoutRedirectPath', $this->t('The path you entered for Redirect logout path is not a valid internal path, internal paths should start with: /, ? or #'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('ldap_sso_auth.settings')
      ->set('ssoExcludedPaths', explode(PHP_EOL, $values['ssoExcludedPaths']))
      ->set('ssoExcludedHosts', explode(PHP_EOL, $values['ssoExcludedHosts']))
      ->set('seamlessLogin', $values['seamlessLogin'])
      ->set('ssoSplitUserRealm', $values['ssoSplitUserRealm'])
      ->set('ssoRemoteUserStripDomainName', $values['ssoRemoteUserStripDomainName'])
      ->set('ssoVariable', $values['ssoVariable'])
      ->set('redirectOnLogout', $values['redirectOnLogout'])
      ->set('logoutRedirectPath', $values['logoutRedirectPath'])
      ->set('enableLoginConfirmationMessage', $values['enableLoginConfirmationMessage'])
      ->save();

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

}
